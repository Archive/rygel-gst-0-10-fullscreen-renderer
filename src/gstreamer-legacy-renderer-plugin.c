/*
 * Copyright (C) 2013 Intel Corporation
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "gstreamer-legacy-renderer-plugin.h"
#include "rygel-playbin-player.h"

#define RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_NAME "GStreamerLegacyRendererPlugin"

enum  {
  RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_DUMMY_PROPERTY
};

#define RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_TITLE "GStreamerLegacy Render Plugin"
#define RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_DESCRIPTION "An GStreamer-legacy Rygel renderer plugin implemented in C."

G_DEFINE_TYPE (RygelGStreamerLegacyRendererPlugin, rygel_gstreamer_legacy_renderer_plugin, RYGEL_TYPE_MEDIA_RENDERER_PLUGIN)

void
module_init (RygelPluginLoader* loader) {
  RygelGStreamerLegacyRendererPlugin* plugin;

  g_return_if_fail (loader != NULL);

  if (rygel_plugin_loader_plugin_disabled (loader, RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_NAME)) {
    g_message ("Plugin '%s' disabled by user. Ignoring.",
      RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_NAME);
    return;
  }

  gst_init (NULL, NULL);

  plugin = rygel_gstreamer_legacy_renderer_plugin_new ();
  rygel_plugin_loader_add_plugin (loader, RYGEL_PLUGIN (plugin));
  g_object_unref (plugin);
}


static RygelGStreamerLegacyRendererPlugin*
rygel_gstreamer_legacy_renderer_plugin_construct (GType object_type) {
  RygelGStreamerLegacyRendererPlugin *self;

  self = (RygelGStreamerLegacyRendererPlugin*) rygel_media_renderer_plugin_construct (object_type,
    RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_NAME, NULL, RYGEL_GSTREAMER_LEGACY_RENDERER_PLUGIN_DESCRIPTION,
    RYGEL_PLUGIN_CAPABILITIES_NONE);

  return self;
}


RygelGStreamerLegacyRendererPlugin*
rygel_gstreamer_legacy_renderer_plugin_new (void) {
  return rygel_gstreamer_legacy_renderer_plugin_construct (RYGEL_GSTREAMER_LEGACY_TYPE_RENDERER_PLUGIN);
}


static RygelMediaPlayer *
rygel_gstreamer_legacy_renderer_plugin_get_player (RygelMediaRendererPlugin* plugin G_GNUC_UNUSED)
{
    return RYGEL_MEDIA_PLAYER (rygel_playbin_player_get_default ());
}

static void
rygel_gstreamer_legacy_renderer_plugin_constructed (GObject* base) {
    RygelMediaRendererPlugin *self = RYGEL_MEDIA_RENDERER_PLUGIN (base);
    RygelPlaybinPlayer *player;
    GList *supported_profiles;

    G_OBJECT_CLASS(rygel_gstreamer_legacy_renderer_plugin_parent_class)->constructed (base);

    player = rygel_playbin_player_get_default ();
    supported_profiles = rygel_playbin_player_get_supported_profiles (player);
    rygel_media_renderer_plugin_set_supported_profiles (self, supported_profiles);
}

static void
rygel_gstreamer_legacy_renderer_plugin_class_init (RygelGStreamerLegacyRendererPluginClass *klass) {
    RygelMediaRendererPluginClass *plugin_class;

    plugin_class = RYGEL_MEDIA_RENDERER_PLUGIN_CLASS (klass);
    plugin_class->get_player = rygel_gstreamer_legacy_renderer_plugin_get_player;
    G_OBJECT_CLASS(plugin_class)->constructed = rygel_gstreamer_legacy_renderer_plugin_constructed;
}


static void
rygel_gstreamer_legacy_renderer_plugin_init (RygelGStreamerLegacyRendererPlugin *self G_GNUC_UNUSED) {
}
