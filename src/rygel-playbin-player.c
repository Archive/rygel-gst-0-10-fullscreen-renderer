/*
 * Copyright (C) 2008 OpenedHand Ltd.
 * Copyright (C) 2009,2010,2011,2012 Nokia Corporation.
 * Copyright (C) 2012 Intel Corporation.
 * Copyright (C) 2012,2013 Openismus GmbH
 *
 * Author: Jorn Baayen <jorn@openedhand.com>
 *         Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 *         Jens Georg <jensg@openismus.com>
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-playbin-player.h"
#include <libgupnp-av/gupnp-av.h>
#include <rygel-renderer.h>
#include <stdlib.h>
#include <string.h>

static void rygel_playbin_player_rygel_media_player_interface_init (RygelMediaPlayerIface *iface);

/**
 * Implementation of RygelMediaPlayer for GStreamer.
 *
 * This class is useful only when implementing Rygel plugins.
 */

G_DEFINE_TYPE_WITH_CODE (RygelPlaybinPlayer,
                         rygel_playbin_player,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RYGEL_TYPE_MEDIA_PLAYER,
                                                rygel_playbin_player_rygel_media_player_interface_init))

#define _vala_assert(expr, msg) if G_LIKELY (expr) ; else g_assertion_message_expr (G_LOG_DOMAIN, __FILE__, __LINE__, G_STRFUNC, msg);

struct _RygelPlaybinPlayerPrivate {
  gboolean duration_hint;
  gboolean is_live;
  gboolean foreign;
  gboolean buffering;
  GstElement *_playbin;
  gchar *_playback_state;
  gchar **_allowed_playback_speeds;
  gint _allowed_playback_speeds_length;
  gint __allowed_playback_speeds_size_;
  gchar *_playback_speed;
  gchar *transfer_mode;
  gboolean uri_update_hint;
  gchar *_uri;
  gchar *_mime_type;
  gchar *_metadata;
  gchar *_content_features;
  GUPnPProtocolInfo *protocol_info;
  GList *_supported_profiles;
};

static RygelMediaPlayerIface* rygel_playbin_player_rygel_media_player_parent_iface = NULL;

#define RYGEL_PLAYBIN_PLAYER_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), RYGEL_TYPE_PLAYBIN_PLAYER, RygelPlaybinPlayerPrivate))
enum  {
  RYGEL_PLAYBIN_PLAYER_DUMMY_PROPERTY,
  RYGEL_PLAYBIN_PLAYER_PLAYBIN,
  RYGEL_PLAYBIN_PLAYER_PLAYBACK_STATE,
  RYGEL_PLAYBIN_PLAYER_ALLOWED_PLAYBACK_SPEEDS,
  RYGEL_PLAYBIN_PLAYER_PLAYBACK_SPEED,
  RYGEL_PLAYBIN_PLAYER_URI,
  RYGEL_PLAYBIN_PLAYER_MIME_TYPE,
  RYGEL_PLAYBIN_PLAYER_METADATA,
  RYGEL_PLAYBIN_PLAYER_CAN_SEEK,
  RYGEL_PLAYBIN_PLAYER_CAN_SEEK_BYTES,
  RYGEL_PLAYBIN_PLAYER_CONTENT_FEATURES,
  RYGEL_PLAYBIN_PLAYER_VOLUME,
  RYGEL_PLAYBIN_PLAYER_DURATION,
  RYGEL_PLAYBIN_PLAYER_SIZE,
  RYGEL_PLAYBIN_PLAYER_POSITION,
  RYGEL_PLAYBIN_PLAYER_BYTE_POSITION,
  RYGEL_PLAYBIN_PLAYER_SUPPORTED_PROFILES
};

#define RYGEL_PLAYBIN_PLAYER_TRANSFER_MODE_STREAMING "Streaming"
#define RYGEL_PLAYBIN_PLAYER_TRANSFER_MODE_INTERACTIVE "Interactive"
#define RYGEL_PLAYBIN_PLAYER_PROTOCOL_INFO_TEMPLATE "http-get:%s:*:%s"

static void rygel_playbin_player_set_playbin (RygelPlaybinPlayer *self, GstElement *value);
static void rygel_playbin_player_setup_playbin (RygelPlaybinPlayer *self);
static gboolean rygel_playbin_player_real_seek (RygelMediaPlayer *base, gint64 time);
static gboolean rygel_playbin_player_real_seek_bytes (RygelMediaPlayer *base, gint64 bytes);
static gchar **rygel_playbin_player_real_get_protocols (RygelMediaPlayer *base, int *result_length1);
static gchar **rygel_playbin_player_real_get_mime_types (RygelMediaPlayer *base, int *result_length1);
static gboolean rygel_playbin_player_is_rendering_image (RygelPlaybinPlayer *self);
static void rygel_playbin_player_bus_handler (GstBus *bus, GstMessage *message, gpointer user_data);
static gchar *rygel_playbin_player_generate_basic_didl (RygelPlaybinPlayer *self);
static void rygel_playbin_player_on_source_setup (GstElement *sender, GstElement *source, gpointer user_data);
static void rygel_playbin_player_on_uri_notify (GObject *sender, GParamSpec *pspec, gpointer user_data);
static void rygel_playbin_player_finalize (GObject *obj);
static void _vala_rygel_playbin_player_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void _vala_rygel_playbin_player_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);

static const gchar *RYGEL_PLAYBIN_PLAYER_PROTOCOLS[2] = {"http-get", "rtsp"};
static const gchar *RYGEL_PLAYBIN_PLAYER_MIME_TYPES[40] = {"audio/mpeg", "application/ogg", "audio/x-vorbis", "audio/x-vorbis+ogg", "audio/ogg", "audio/x-ms-wma", "audio/x-ms-asf", "audio/x-flac", "audio/x-flac+ogg", "audio/flac", "audio/mp4", "audio/vnd.dlna.adts", "audio/x-mod", "audio/x-wav", "audio/x-ac3", "audio/x-m4a", "audio/L16;rate=44100;channels=2", "audio/L16;rate=44100;channels=1", "audio/L16;channels=2;rate=44100", "audio/L16;channels=1;rate=44100", "audio/L16;rate=44100", "image/jpeg", "image/png", "video/x-theora", "video/x-theora+ogg", "video/x-oggm", "video/ogg", "video/x-dirac", "video/x-wmv", "video/x-wma", "video/x-msvideo", "video/x-3ivx", "video/x-3ivx", "video/x-matroska", "video/x-mkv", "video/mpeg", "video/mp4", "video/x-ms-asf", "video/x-xvid", "video/x-ms-wmv"};


static RygelPlaybinPlayer*
rygel_playbin_player_new (void) {
  RygelPlaybinPlayer *self;

  self = RYGEL_PLAYBIN_PLAYER (g_object_new (RYGEL_TYPE_PLAYBIN_PLAYER, NULL));

  /* TODO: This should really be done via a construct property. */
  GstElement *playbin = gst_element_factory_make ("playbin2", NULL);
  if (playbin) {
    gst_object_ref_sink (playbin);
  }

  rygel_playbin_player_set_playbin (self, playbin);
  gst_object_unref (playbin);

  self->priv->foreign = FALSE;
  rygel_playbin_player_setup_playbin (self);

  return self;
}

static RygelPlaybinPlayer* rygel_playbin_player_player = NULL;

RygelPlaybinPlayer*
rygel_playbin_player_get_default (void) {
  if (!rygel_playbin_player_player) {
    rygel_playbin_player_player = rygel_playbin_player_new ();
  }

  g_object_ref (rygel_playbin_player_player);
  return rygel_playbin_player_player;
}


static gboolean
rygel_playbin_player_real_seek (RygelMediaPlayer *base, gint64 time) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  g_return_val_if_fail (self, FALSE);
  g_return_val_if_fail (self->priv->_playbin, FALSE);

  return gst_element_seek (self->priv->_playbin, 1.0, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH, GST_SEEK_TYPE_SET, time * GST_USECOND, GST_SEEK_TYPE_NONE, (gint64) (-1));
}

static gboolean
rygel_playbin_player_real_seek_bytes (RygelMediaPlayer *base, gint64 bytes) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  g_return_val_if_fail (self, FALSE);
  g_return_val_if_fail (self->priv->_playbin, FALSE);

  return gst_element_seek (self->priv->_playbin, 1.0, GST_FORMAT_BYTES, GST_SEEK_FLAG_FLUSH, GST_SEEK_TYPE_SET, bytes, GST_SEEK_TYPE_NONE, (gint64) (-1));
}


static gchar**
rygel_playbin_player_real_get_protocols (RygelMediaPlayer *base, int *result_length) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  int length = 0;
  gchar **result = NULL;
  int i = 0;

  g_return_val_if_fail (self, NULL);

  length =  G_N_ELEMENTS (RYGEL_PLAYBIN_PLAYER_PROTOCOLS);

  if (result_length) {
    *result_length = length;
  }

  result = g_new0 (gchar*, length + 1);
  for (i = 0; i < length; i++) {
    result[i] = g_strdup (RYGEL_PLAYBIN_PLAYER_PROTOCOLS[i]);
  }

  return result;
}

static gchar**
rygel_playbin_player_real_get_mime_types (RygelMediaPlayer *base, int *result_length) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  int length = 0;
  gchar **result = NULL;
  int i = 0;

  g_return_val_if_fail (self, NULL);

  length =  G_N_ELEMENTS (RYGEL_PLAYBIN_PLAYER_MIME_TYPES);

  if (result_length) {
    *result_length = length;
  }

  result = g_new0 (gchar*, length + 1);
  for (i = 0; i < length; i++) {
    result[i] = g_strdup (RYGEL_PLAYBIN_PLAYER_MIME_TYPES[i]);
  }

  return result;
}


static gboolean
rygel_playbin_player_is_rendering_image (RygelPlaybinPlayer *self) {
  GstElement* typefind = NULL;
  GstCaps *caps = NULL;
  GstStructure *structure = NULL;
  const gchar *name = NULL;
  gboolean result;

  g_return_val_if_fail (self, FALSE);
  g_return_val_if_fail (self->priv->_playbin, FALSE);

  typefind = gst_bin_get_by_name (GST_BIN (self->priv->_playbin), "typefind");
  g_return_val_if_fail (typefind, FALSE);

  g_object_get (typefind, "caps", &caps, NULL);
  g_return_val_if_fail (caps, FALSE);

  structure = gst_caps_get_structure (caps, 0);
  g_return_val_if_fail (structure, FALSE);

  name = gst_structure_get_name (structure);
  result = (g_strcmp0 (name, "image/jpeg") == 0) ||
    (g_strcmp0 (name, "image/png") == 0);

  gst_caps_unref (caps);
  gst_object_unref (typefind);

  return result;
}

static void
rygel_playbin_player_bus_handler (GstBus *bus, GstMessage *message, gpointer user_data) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (user_data);
  g_return_if_fail (self);

  g_return_if_fail (bus);
  g_return_if_fail (message);

  switch (message->type) {
    case GST_MESSAGE_DURATION:
    {
      GstFormat format = GST_FORMAT_TIME;
      if (gst_element_query_duration (self->priv->_playbin, &format, NULL)) {
        g_object_notify (G_OBJECT (self), "duration");
      }

      break;
    }
    case GST_MESSAGE_STATE_CHANGED:
    {
      if (message->src == GST_OBJECT (self->priv->_playbin)) {
        GstState old_state = 0;
        GstState new_state = 0;
        GstState pending = 0;
        gst_message_parse_state_changed (message, &old_state, &new_state, &pending);
        if (old_state == GST_STATE_READY && new_state == GST_STATE_PAUSED) {
          if (self->priv->uri_update_hint) {
            self->priv->uri_update_hint = FALSE;

            gchar *current_uri = NULL;
            g_object_get (self->priv->_playbin, "uri", &current_uri, NULL);

            if (g_strcmp0 (self->priv->_uri, current_uri) != 0
              && g_strcmp0 (current_uri, "") != 0) {

              // uri changed externally
              if (self->priv->_uri)
                g_free (self->priv->_uri);

              gchar *uri = NULL;
              g_object_get (self->priv->_playbin, "uri", &uri, NULL);
              self->priv->_uri = uri;
              g_object_notify (G_OBJECT (self), "uri");

              gchar *metadata = rygel_playbin_player_generate_basic_didl (self);
              rygel_media_player_set_metadata (RYGEL_MEDIA_PLAYER (self), metadata);
              g_free (metadata);
            }

            g_free (current_uri);
          }
        }

        if (pending == GST_STATE_VOID_PENDING
          && !self->priv->buffering) {
          switch (new_state) {
            case GST_STATE_PAUSED:
            {
              rygel_media_player_set_playback_state (RYGEL_MEDIA_PLAYER (self), "PAUSED_PLAYBACK");
              break;
            }
            case GST_STATE_NULL:
            {
              rygel_media_player_set_playback_state (RYGEL_MEDIA_PLAYER (self), "STOPPED");
              break;
            }
            case GST_STATE_PLAYING:
            {
              rygel_media_player_set_playback_state (RYGEL_MEDIA_PLAYER (self), "PLAYING");
              break;
            }
            default:
            {
              break;
            }
          }
        }

        if (old_state == GST_STATE_PAUSED
          && new_state == GST_STATE_PLAYING) {
          self->priv->buffering = FALSE;
          rygel_media_player_set_playback_state (RYGEL_MEDIA_PLAYER (self), "PLAYING");
        }
      }

      break;
    }
    case GST_MESSAGE_BUFFERING:
    {
      // Assume the original application takes care of this.
      if (!self->priv->is_live || self->priv->foreign) {
        gint percent = 0;
        gst_message_parse_buffering (message, &percent);
 
        if (percent < 100) {
          self->priv->buffering = TRUE;
          gst_element_set_state (self->priv->_playbin, GST_STATE_PAUSED);
        } else {
          gst_element_set_state (self->priv->_playbin, GST_STATE_PLAYING);
        }
      }

      break;
    }
    case GST_MESSAGE_CLOCK_LOST:
    {
      // Assume the original application takes care of this.
      if (!self->priv->foreign) {
        gst_element_set_state (self->priv->_playbin, GST_STATE_PAUSED);
        gst_element_set_state (self->priv->_playbin, GST_STATE_PLAYING);
      }

      break;
    }
    case GST_MESSAGE_EOS:
    {
      if(!rygel_playbin_player_is_rendering_image (self)) {
        g_debug ("rygel-playbin-player.c: EOS");
        rygel_media_player_set_playback_state (RYGEL_MEDIA_PLAYER (self), "STOPPED");
      } else {
        g_debug ("rygel-playbin-player.c: Content is image. Ignoring EOS");
      }

      break;
    }
    case GST_MESSAGE_ERROR:
    {
      GError *error = NULL;
      gchar *error_message = NULL;
      gchar *name = gst_object_get_name (GST_OBJECT (self->priv->_playbin));

      gst_message_parse_error (message, &error, &error_message);
      g_warning ("Error from GStreamer element %s: %s (%s)",
                 name,
                 error->message,
                 error_message);
      g_free (name);
      g_error_free (error);
      g_free (error_message);
      g_warning ("Going to STOPPED state");

      rygel_media_player_set_playback_state (RYGEL_MEDIA_PLAYER (self), "STOPPED");

      break;
    }
    default:
    break;
  }
}


/**
 * Generate basic DIDLLite information.
 *
 * This is used when the URI gets changed externally. DLNA requires that a
 * minimum DIDLLite is always present if the URI is not empty.
 */
static gchar*
rygel_playbin_player_generate_basic_didl (RygelPlaybinPlayer *self) {
  GUPnPDIDLLiteWriter *writer = NULL;
  GUPnPDIDLLiteItem *item = NULL;
  GUPnPDIDLLiteResource *resource = NULL;
  GFile *file = NULL;
  gchar *basename = NULL;
  gchar *result = NULL;

  g_return_val_if_fail (self, NULL);

  writer = gupnp_didl_lite_writer_new (NULL);

  item = gupnp_didl_lite_writer_add_item (writer);
  gupnp_didl_lite_object_set_id ((GUPnPDIDLLiteObject*) item, "1");
  gupnp_didl_lite_object_set_parent_id ((GUPnPDIDLLiteObject*) item, "-1");
  gupnp_didl_lite_object_set_upnp_class ((GUPnPDIDLLiteObject*) item, "object.item");

  resource = gupnp_didl_lite_object_add_resource ((GUPnPDIDLLiteObject*) item);
  gupnp_didl_lite_resource_set_uri (resource, self->priv->_uri);

  file = g_file_new_for_uri (self->priv->_uri);
  basename = g_file_get_basename (file);
  gupnp_didl_lite_object_set_title ((GUPnPDIDLLiteObject*) item, basename);
  g_free (basename);

  result = gupnp_didl_lite_writer_get_string (writer);

  g_object_unref (file);
  g_object_unref (resource);
  g_object_unref (item);
  g_object_unref (writer);

  return result;
}


static void
rygel_playbin_player_on_source_setup (GstElement *sender G_GNUC_UNUSED, GstElement *source, gpointer user_data) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (user_data);

  GstStructure *structure = NULL;

  g_return_if_fail (self);
  g_return_if_fail (source);

  //We do not use GST_IS_SOUP_HTTP_SRC(), to avoid a build-time dependency on gst-plugins-good,
  //though it will be needed at runtime.
  if ((g_strcmp0 (G_OBJECT_TYPE_NAME (source), "GstSoupHTTPSrc") == 0) &&
    self->priv->transfer_mode) {
    g_debug ("rygel-playbin-player.c: Setting transfer mode to %s", self->priv->transfer_mode);
    
    structure = gst_structure_empty_new ("Extra Headers");
    gst_structure_set (structure, "transferMode.dlna.org", G_TYPE_STRING, self->priv->transfer_mode, NULL);

    g_object_set (source, "extra-headers", structure, NULL);

    gst_structure_free (structure);
  }
}

static void
rygel_playbin_player_on_uri_notify (GObject *sender G_GNUC_UNUSED, GParamSpec *pspec, gpointer user_data) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (user_data);

  g_return_if_fail (self);
  g_return_if_fail (pspec);

  self->priv->uri_update_hint = TRUE;
}

static void
rygel_playbin_player_setup_playbin (RygelPlaybinPlayer *self) {
  GstBus *bus = NULL;

  g_return_if_fail (self);
  g_return_if_fail (self->priv->_playbin);

  self->priv->duration_hint = FALSE;

  /* Needed to get "Stop" events from the playbin.
   * We can do this because we have a bus watch.
   */
  self->priv->is_live = FALSE;

  g_object_set (self->priv->_playbin, "auto-flush-bus", FALSE, NULL);
  g_assert (self->priv->_playbin);

  g_signal_connect_object (self->priv->_playbin, "source_setup", (GCallback) rygel_playbin_player_on_source_setup, self, 0);
  g_signal_connect_object (self->priv->_playbin, "notify::uri", (GCallback) rygel_playbin_player_on_uri_notify, self, 0);

  /* Bus handler */
  bus = gst_element_get_bus (self->priv->_playbin);
  g_return_if_fail (bus);

  gst_bus_add_signal_watch (bus);
  g_signal_connect_object (bus, "message", (GCallback) rygel_playbin_player_bus_handler, self, 0);

  gst_object_unref (bus);
}


GstElement*
rygel_playbin_player_get_playbin (RygelPlaybinPlayer *self) {
  g_return_val_if_fail (self, NULL);

  /* TODO: Shouldn't we ref this, like other GStreamer getters? */
  return self->priv->_playbin;
}

static void
rygel_playbin_player_set_playbin (RygelPlaybinPlayer *self, GstElement *value) {
  g_return_if_fail (self);

  if(self->priv->_playbin)
    gst_object_unref (self->priv->_playbin);

  gst_object_ref (value);
  self->priv->_playbin = value;

  g_object_notify (G_OBJECT (self), "playbin");
}


static gchar*
rygel_playbin_player_real_get_playback_state (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, NULL);

  return g_strdup (self->priv->_playback_state);
}


static void
rygel_playbin_player_real_set_playback_state (RygelMediaPlayer *base, const gchar *value) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_if_fail (self);

  GstState state = 0;
  GstState pending = 0;
  gst_element_get_state (self->priv->_playbin, &state, &pending, (GstClockTime) GST_MSECOND);

  g_debug ("rygel-playbin-player.c: Changing playback state to %s.", value);

  if (g_strcmp0 (value, "STOPPED") == 0) {
    if (self->priv->_playback_state)
      g_free (self->priv->_playback_state);

    if (state != GST_STATE_NULL ||
      pending != GST_STATE_VOID_PENDING) {
      self->priv->_playback_state = g_strdup ("TRANSITIONING");
      gst_element_set_state (self->priv->_playbin, GST_STATE_NULL);
    } else {
      self->priv->_playback_state = g_strdup (value);
    }
  } else if (g_strcmp0 (value, "PAUSED_PLAYBACK") == 0) {
    if (self->priv->_playback_state)
      g_free (self->priv->_playback_state);

    if (state != GST_STATE_PAUSED ||
      pending != GST_STATE_VOID_PENDING) {
      self->priv->_playback_state = g_strdup ("TRANSITIONING");
      gst_element_set_state (self->priv->_playbin, GST_STATE_PAUSED);
    } else {
      self->priv->_playback_state = g_strdup (value);
    }
  } else if (g_strcmp0 (value, "PLAYING") == 0) {
    if (self->priv->_playback_state)
      g_free (self->priv->_playback_state);

    if (state != GST_STATE_PLAYING ||
      pending != GST_STATE_VOID_PENDING) {
      self->priv->_playback_state = g_strdup ("TRANSITIONING");

      /* This needs a check if GStreamer and DLNA agree on
       * the "liveness" of the source (s0/sn increase in
       * protocol info.
       */
      self->priv->is_live = 
        (gst_element_set_state (self->priv->_playbin, GST_STATE_PLAYING) == GST_STATE_CHANGE_NO_PREROLL);
    } else {
      self->priv->_playback_state = g_strdup (value);
    }
  } else if (g_strcmp0 (value, "EOS") == 0) {
    if (self->priv->_playback_state)
      g_free (self->priv->_playback_state);

    self->priv->_playback_state = g_strdup (value);
  }

  g_object_notify (G_OBJECT (self), "playback-state");
}


static gchar**
rygel_playbin_player_real_get_allowed_playback_speeds (RygelMediaPlayer *base, int *result_length) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  gchar **result = NULL;
  int i = 0;

  g_return_val_if_fail (self, NULL);

  if (result_length) {
    *result_length = self->priv->_allowed_playback_speeds_length;
  }

  result = g_new0 (gchar*, self->priv->_allowed_playback_speeds_length + 1);
  for (i = 0; i < self->priv->_allowed_playback_speeds_length; i++) {
    result[i] = g_strdup (self->priv->_allowed_playback_speeds[i]);
  }

  return result;
}


static gchar*
rygel_playbin_player_real_get_playback_speed (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, NULL);

  return g_strdup (self->priv->_playback_speed);
}


static void rygel_playbin_player_real_set_playback_speed (RygelMediaPlayer *base, const gchar *value) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_if_fail (self);

  if (self->priv->_playback_speed)
    g_free (self->priv->_playback_speed);

  self->priv->_playback_speed = g_strdup (value);

  g_object_notify (G_OBJECT (self), "playback-speed");
}


static gchar*
rygel_playbin_player_real_get_uri (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, NULL);

  return g_strdup (self->priv->_uri);
}


static void
rygel_playbin_player_real_set_uri (RygelMediaPlayer *base, const gchar *value) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_if_fail (self);

  if (self->priv->_uri)
    g_free (self->priv->_uri);
  self->priv->_uri = g_strdup (value);

  gst_element_set_state (self->priv->_playbin, GST_STATE_READY);
  g_object_set (self->priv->_playbin, "uri", value, NULL);

  if (g_strcmp0 (value, "") != 0) {
    if (g_strcmp0 (self->priv->_playback_state, "NO_MEDIA_PRESENT") != 0) {
      if (self->priv->_playback_state)
        g_free (self->priv->_playback_state);

      self->priv->_playback_state = g_strdup ("STOPPED");
      g_object_notify (G_OBJECT (self), "playback-state");
    } else if (g_strcmp0 (self->priv->_playback_state, "STOPPED") != 0) {
    } else if (g_strcmp0 (self->priv->_playback_state, "PAUSED_PLAYBACK") != 0) {
      self->priv->is_live = 
        (gst_element_set_state (self->priv->_playbin, GST_STATE_PAUSED) == GST_STATE_CHANGE_NO_PREROLL);
    } else if (g_strcmp0 (self->priv->_playback_state, "EOS") != 0 ||
      g_strcmp0 (self->priv->_playback_state, "PLAYING") != 0) {
      /* This needs a check if GStreamer and DLNA agree on
       * the "liveness" of the source (s0/sn increase in
       * protocol info.
       */
      self->priv->is_live = 
        (gst_element_set_state (self->priv->_playbin, GST_STATE_PLAYING) == GST_STATE_CHANGE_NO_PREROLL);
    }
  } else {
   if (self->priv->_playback_state)
     g_free (self->priv->_playback_state);

    self->priv->_playback_state = g_strdup ("NO_MEDIA_PRESENT");
    g_object_notify (G_OBJECT (self), "playback-state");
  }

  g_debug ("rygel-playbin-player.c: URI set to %s.", value);
}


static gchar*
rygel_playbin_player_real_get_mime_type (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, NULL);

  return g_strdup (self->priv->_mime_type);
}


static void
rygel_playbin_player_real_set_mime_type (RygelMediaPlayer *base, const gchar *value) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_if_fail (self);

  if (self->priv->_mime_type)
    g_free (self->priv->_mime_type);

  self->priv->_mime_type = g_strdup (value);

  g_object_notify (G_OBJECT (self), "mime-type");
}


static gchar*
rygel_playbin_player_real_get_metadata (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, NULL);

  return g_strdup (self->priv->_metadata);
}


static void
rygel_playbin_player_real_set_metadata (RygelMediaPlayer *base, const gchar *value) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_if_fail (self);

  if (self->priv->_metadata)
    g_free (self->priv->_metadata);

  self->priv->_metadata = g_strdup (value);

  g_object_notify (G_OBJECT (self), "metadata");
}


static gboolean
rygel_playbin_player_real_get_can_seek (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, FALSE);

  return (g_strcmp0 (self->priv->transfer_mode, RYGEL_PLAYBIN_PLAYER_TRANSFER_MODE_INTERACTIVE) != 0) &&
    g_str_has_prefix (self->priv->_mime_type, "image/");
}

static gboolean
rygel_playbin_player_real_get_can_seek_bytes (RygelMediaPlayer *base) {
  return rygel_playbin_player_real_get_can_seek (base);
}

static gchar*
rygel_playbin_player_real_get_content_features (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, NULL);

  return g_strdup (self->priv->_content_features);
}


static void
rygel_playbin_player_real_set_content_features (RygelMediaPlayer *base, const gchar *value) {
  GError *inner_error = NULL;
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_if_fail (self);

  gchar *mime_type = rygel_media_player_get_mime_type (RYGEL_MEDIA_PLAYER (self));
  gchar *pi_string = g_strdup_printf (RYGEL_PLAYBIN_PLAYER_PROTOCOL_INFO_TEMPLATE,
    mime_type, value);
  g_free (mime_type);

  if (self->priv->protocol_info)
    g_object_unref (self->priv->protocol_info);

  self->priv->protocol_info = gupnp_protocol_info_new_from_string (pi_string, &inner_error);
  if (inner_error) {
    if (self->priv->protocol_info)
     g_object_unref (self->priv->protocol_info);
    self->priv->protocol_info = NULL;

    if (self->priv->transfer_mode)
      g_free (self->priv->transfer_mode);
    self->priv->transfer_mode = NULL;

    g_error_free (inner_error);
  }

  if (self->priv->transfer_mode)
    g_free (self->priv->transfer_mode);

  GUPnPDLNAFlags flags = gupnp_protocol_info_get_dlna_flags (self->priv->protocol_info);
  if ((flags & GUPNP_DLNA_FLAGS_INTERACTIVE_TRANSFER_MODE) == GUPNP_DLNA_FLAGS_INTERACTIVE_TRANSFER_MODE) {
    self->priv->transfer_mode = g_strdup (RYGEL_PLAYBIN_PLAYER_TRANSFER_MODE_INTERACTIVE);
  } else if ((flags & GUPNP_DLNA_FLAGS_STREAMING_TRANSFER_MODE) == GUPNP_DLNA_FLAGS_STREAMING_TRANSFER_MODE) {
    self->priv->transfer_mode = g_strdup (RYGEL_PLAYBIN_PLAYER_TRANSFER_MODE_STREAMING);
  } else {
     self->priv->transfer_mode = NULL;
  }

  if (self->priv->_content_features)
    g_free (self->priv->_content_features);
  self->priv->_content_features = g_strdup (value);

  g_free (pi_string);
  g_object_notify (G_OBJECT (self), "content-features");
}

static gdouble
rygel_playbin_player_real_get_volume (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_val_if_fail (self, 0);
  g_return_val_if_fail (self->priv->_playbin, 0);

  gdouble result = 0;
  g_object_get (self->priv->_playbin, "volume", &result, NULL);
  return result;
}


static void
rygel_playbin_player_real_set_volume (RygelMediaPlayer *base, gdouble value) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);

  g_return_if_fail (self);

  g_object_set (self->priv->_playbin, "volume", value, NULL);

  g_debug ("rygel-playbin-player.c: volume set to %f.", value);
  g_object_notify (G_OBJECT (self), "volume");
}


static gint64
rygel_playbin_player_real_get_duration (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  GstFormat format = GST_FORMAT_TIME;
  gint64 pos = 0LL;

  g_return_val_if_fail (self, 0LL);
  g_return_val_if_fail (self->priv->_playbin, 0LL);

  if (gst_element_query_duration (self->priv->_playbin, &format, &pos)) {
    return pos / GST_USECOND;
  } else {
    return 0LL;
  }
}

static gint64
rygel_playbin_player_real_get_size (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  GstFormat format = GST_FORMAT_BYTES;
  gint64 pos = 0LL;

  g_return_val_if_fail (self, 0LL);
  g_return_val_if_fail (self->priv->_playbin, 0LL);

  if (gst_element_query_duration (self->priv->_playbin, &format, &pos)) {
    return pos / GST_USECOND;
  } else {
    return 0LL;
  }
}

static gint64
rygel_playbin_player_real_get_position (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  GstFormat format = GST_FORMAT_TIME;
  gint64 pos = 0LL;

  g_return_val_if_fail (self, 0LL);
  g_return_val_if_fail (self->priv->_playbin, 0LL);

  if (gst_element_query_position (self->priv->_playbin, &format, &pos)) {
    return pos / GST_USECOND;
  } else {
    return 0LL;
  }
}

static gint64
rygel_playbin_player_real_get_byte_position (RygelMediaPlayer *base) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (base);
  GstFormat format = GST_FORMAT_BYTES;
  gint64 pos = 0LL;

  g_return_val_if_fail (self, 0LL);
  g_return_val_if_fail (self->priv->_playbin, 0LL);

  if (gst_element_query_position (self->priv->_playbin, &format, &pos)) {
    return pos / GST_USECOND;
  } else {
    return 0LL;
  }
}

GList *
rygel_playbin_player_get_supported_profiles (RygelPlaybinPlayer *self) {
  if (self->priv->_supported_profiles == NULL) {
    RygelDLNAProfile *profile = NULL;

    profile = rygel_dlna_profile_new ("JPEG_SM", "image/jpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("JPEG_MED", "image/jpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("JPEG_LRG", "image/jpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("PNG_LRG", "image/png");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("MP3", "audio/mpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("MP3X", "audio/mpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("AAC_ADTS_320", "audio/vnd.dlna.adts");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("AAC_ISO_320", "audio/mp4");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("AAC_ISO_320", "audio/3gpp");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("LPCM", "audio/l16;rate=44100;channels=2");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("LPCM", "audio/l16;rate=44100;channels=1");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("WMABASE", "audio/x-ms-wma");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("WMAFULL", "audio/x-ms-wma");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("WMAPRO", "audio/x-ms-wma");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("MPEG_TS_SD_EU_ISO", "video/mpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("MPEG_TS_SD_NA_ISO", "video/mpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("MPEG_TS_HD_NA_ISO", "video/mpeg");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
    profile = rygel_dlna_profile_new ("AVC_MP4_BL_CIF15_AAC_520", "video/mp4");
    self->priv->_supported_profiles = g_list_prepend (self->priv->_supported_profiles, profile);
  }

  return self->priv->_supported_profiles;
}


static void rygel_playbin_player_class_init (RygelPlaybinPlayerClass *klass) {
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  rygel_playbin_player_parent_class = g_type_class_peek_parent (klass);
  g_type_class_add_private (klass, sizeof (RygelPlaybinPlayerPrivate));

  gobject_class->get_property = _vala_rygel_playbin_player_get_property;
  gobject_class->set_property = _vala_rygel_playbin_player_set_property;
  gobject_class->finalize = rygel_playbin_player_finalize;

  g_object_class_install_property (gobject_class,
    RYGEL_PLAYBIN_PLAYER_PLAYBIN,
    g_param_spec_object ("playbin", "playbin", "playbin", GST_TYPE_ELEMENT,
      G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE));

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_PLAYBACK_STATE,
                                    "playback-state");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_URI,
                                    "uri");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_MIME_TYPE,
                                    "mime-type");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_METADATA,
                                    "metadata");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_CAN_SEEK,
                                    "can-seek");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_CAN_SEEK_BYTES,
                                    "can-seek-bytes");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_CONTENT_FEATURES,
                                    "content-features");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_VOLUME,
                                    "volume");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_DURATION,
                                    "duration");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_SIZE,
                                    "size");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_POSITION,
                                    "position");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_BYTE_POSITION,
                                    "byte-position");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_PLAYBACK_SPEED,
                                    "playback-speed");

  g_object_class_override_property (gobject_class,
                                    RYGEL_PLAYBIN_PLAYER_ALLOWED_PLAYBACK_SPEEDS,
                                    "allowed-playback-speeds");
}


static void rygel_playbin_player_rygel_media_player_interface_init (RygelMediaPlayerIface *iface) {
  rygel_playbin_player_rygel_media_player_parent_iface = g_type_interface_peek_parent (iface);

  iface->seek = (gboolean (*)(RygelMediaPlayer*, gint64)) rygel_playbin_player_real_seek;
  iface->seek_bytes = rygel_playbin_player_real_seek_bytes;
  iface->get_protocols = (gchar **(*)(RygelMediaPlayer*, int*)) rygel_playbin_player_real_get_protocols;
  iface->get_mime_types = (gchar **(*)(RygelMediaPlayer*, int*)) rygel_playbin_player_real_get_mime_types;
  iface->get_playback_state = rygel_playbin_player_real_get_playback_state;
  iface->set_playback_state = rygel_playbin_player_real_set_playback_state;
  iface->get_allowed_playback_speeds = rygel_playbin_player_real_get_allowed_playback_speeds;
  iface->get_playback_speed = rygel_playbin_player_real_get_playback_speed;
  iface->set_playback_speed = rygel_playbin_player_real_set_playback_speed;
  iface->get_uri = rygel_playbin_player_real_get_uri;
  iface->set_uri = rygel_playbin_player_real_set_uri;
  iface->get_mime_type = rygel_playbin_player_real_get_mime_type;
  iface->set_mime_type = rygel_playbin_player_real_set_mime_type;
  iface->get_metadata = rygel_playbin_player_real_get_metadata;
  iface->set_metadata = rygel_playbin_player_real_set_metadata;
  iface->get_can_seek = rygel_playbin_player_real_get_can_seek;
  iface->get_can_seek_bytes = rygel_playbin_player_real_get_can_seek_bytes;
  iface->get_content_features = rygel_playbin_player_real_get_content_features;
  iface->set_content_features = rygel_playbin_player_real_set_content_features;
  iface->get_volume = rygel_playbin_player_real_get_volume;
  iface->set_volume = rygel_playbin_player_real_set_volume;
  iface->get_duration = rygel_playbin_player_real_get_duration;
  iface->get_position = rygel_playbin_player_real_get_position;
  iface->get_byte_position = rygel_playbin_player_real_get_byte_position;
  iface->get_size = rygel_playbin_player_real_get_size;
}


static void rygel_playbin_player_init (RygelPlaybinPlayer *self) {
  g_return_if_fail (self);

  self->priv = RYGEL_PLAYBIN_PLAYER_GET_PRIVATE (self);

  self->priv->_playback_state = g_strdup ("NO_MEDIA_PRESENT");
  self->priv->transfer_mode = NULL;
  self->priv->uri_update_hint = FALSE;
  self->priv->_uri = NULL;
  self->priv->_mime_type = NULL;
  self->priv->_metadata = NULL;
  self->priv->_content_features = NULL;
  self->priv->_allowed_playback_speeds = g_strsplit ("1", ",", -1);
  self->priv->_allowed_playback_speeds_length = 1;
  self->priv->_playback_speed = g_strdup ("1");
}


static void rygel_playbin_player_finalize (GObject *obj) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (obj);
 
  g_return_if_fail (self);

  gst_object_unref (self->priv->_playbin);

  if (self->priv->_playback_state)    
    g_free (self->priv->_playback_state);

  if (self->priv->_allowed_playback_speeds) {
    int i;
    for (i = 0; i < self->priv->_allowed_playback_speeds_length; i = i + 1) {
      g_free (self->priv->_allowed_playback_speeds[i]);
    }
    g_free (self->priv->_allowed_playback_speeds);
  }

  if (self->priv->_playback_speed)
    g_free (self->priv->_playback_speed);

  if (self->priv->transfer_mode)
    g_free (self->priv->transfer_mode);

  if (self->priv->_uri)
    g_free (self->priv->_uri);

  if (self->priv->_mime_type)
    g_free (self->priv->_mime_type);
  
  if (self->priv->_metadata)
    g_free (self->priv->_metadata);
  
  if (self->priv->_content_features)
    g_free (self->priv->_content_features);
  
  if (self->priv->protocol_info)
    g_object_unref (self->priv->protocol_info);

  G_OBJECT_CLASS (rygel_playbin_player_parent_class)->finalize (obj);
}


static void _vala_rygel_playbin_player_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (object);

  g_return_if_fail (self);

  switch (property_id) {
    case RYGEL_PLAYBIN_PLAYER_PLAYBIN:
      g_value_set_object (value, rygel_playbin_player_get_playbin (self));
      break;
    case RYGEL_PLAYBIN_PLAYER_PLAYBACK_STATE:
      g_value_take_string (value, rygel_media_player_get_playback_state (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_ALLOWED_PLAYBACK_SPEEDS:
    {
      int length = 0;
      g_value_take_boxed (value, rygel_media_player_get_allowed_playback_speeds (RYGEL_MEDIA_PLAYER (self), &length));
      break;
    }
    case RYGEL_PLAYBIN_PLAYER_PLAYBACK_SPEED:
      g_value_take_string (value, rygel_media_player_get_playback_speed (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_URI:
      g_value_take_string (value, rygel_media_player_get_uri (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_MIME_TYPE:
      g_value_take_string (value, rygel_media_player_get_mime_type (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_METADATA:
      g_value_take_string (value, rygel_media_player_get_metadata (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_CAN_SEEK:
      g_value_set_boolean (value, rygel_media_player_get_can_seek (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_CAN_SEEK_BYTES:
      g_value_set_boolean (value, rygel_media_player_get_can_seek_bytes (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_CONTENT_FEATURES:
      g_value_take_string (value, rygel_media_player_get_content_features (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_VOLUME:
      g_value_set_double (value, rygel_media_player_get_volume (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_DURATION:
      g_value_set_int64 (value, rygel_media_player_get_duration (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_SIZE:
      g_value_set_int64 (value, rygel_media_player_get_size (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_POSITION:
      g_value_set_int64 (value, rygel_media_player_get_position (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_BYTE_POSITION:
      g_value_set_int64 (value, rygel_media_player_get_byte_position (RYGEL_MEDIA_PLAYER (self)));
      break;
    case RYGEL_PLAYBIN_PLAYER_SUPPORTED_PROFILES:
      g_value_set_pointer (value, rygel_playbin_player_get_supported_profiles (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void _vala_rygel_playbin_player_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec) {
  RygelPlaybinPlayer *self = RYGEL_PLAYBIN_PLAYER (object);

  g_return_if_fail (self);

  switch (property_id) {
    case RYGEL_PLAYBIN_PLAYER_PLAYBIN:
      rygel_playbin_player_set_playbin (self, g_value_get_object (value));
      break;
    case RYGEL_PLAYBIN_PLAYER_PLAYBACK_STATE:
      rygel_media_player_set_playback_state (RYGEL_MEDIA_PLAYER (self), g_value_get_string (value));
      break;
    case RYGEL_PLAYBIN_PLAYER_PLAYBACK_SPEED:
      rygel_media_player_set_playback_speed (RYGEL_MEDIA_PLAYER (self), g_value_get_string (value));
      break;
    case RYGEL_PLAYBIN_PLAYER_URI:
      rygel_media_player_set_uri (RYGEL_MEDIA_PLAYER (self), g_value_get_string (value));
      break;
    case RYGEL_PLAYBIN_PLAYER_MIME_TYPE:
      rygel_media_player_set_mime_type (RYGEL_MEDIA_PLAYER (self), g_value_get_string (value));
      break;
    case RYGEL_PLAYBIN_PLAYER_METADATA:
      rygel_media_player_set_metadata (RYGEL_MEDIA_PLAYER (self), g_value_get_string (value));
      break;
    case RYGEL_PLAYBIN_PLAYER_CONTENT_FEATURES:
      rygel_media_player_set_content_features (RYGEL_MEDIA_PLAYER (self), g_value_get_string (value));
      break;
    case RYGEL_PLAYBIN_PLAYER_VOLUME:
      rygel_media_player_set_volume (RYGEL_MEDIA_PLAYER (self), g_value_get_double (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

