/*
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_FULLSCREEN_RENDERER_PLAYBIN_RENDERER_H__
#define __RYGEL_GST_0_10_FULLSCREEN_RENDERER_PLAYBIN_RENDERER_H__

#include <glib.h>
#include <glib-object.h>
#include <rygel-renderer.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define RYGEL_TYPE_PLAYBIN_RENDERER (rygel_playbin_renderer_get_type ())
#define RYGEL_PLAYBIN_RENDERER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_PLAYBIN_RENDERER, RygelPlaybinRenderer))
#define RYGEL_PLAYBIN_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_PLAYBIN_RENDERER, RygelPlaybinRendererClass))
#define RYGEL_IS_PLAYBIN_RENDERER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_PLAYBIN_RENDERER))
#define RYGEL_IS_PLAYBIN_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_PLAYBIN_RENDERER))
#define RYGEL_PLAYBIN_RENDERER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_PLAYBIN_RENDERER, RygelPlaybinRendererClass))

typedef struct _RygelPlaybinRenderer RygelPlaybinRenderer;
typedef struct _RygelPlaybinRendererClass RygelPlaybinRendererClass;
typedef struct _RygelPlaybinRendererPrivate RygelPlaybinRendererPrivate;

struct _RygelPlaybinRenderer {
  RygelMediaRenderer parent_instance;
  RygelPlaybinRendererPrivate *priv;
};

struct _RygelPlaybinRendererClass {
  RygelMediaRendererClass parent_class;
};

GType
rygel_playbin_renderer_get_type (void) G_GNUC_CONST;

RygelPlaybinRenderer *
rygel_playbin_renderer_new (const gchar *title);

GstElement*
rygel_playbin_renderer_get_playbin (RygelPlaybinRenderer *self);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_FULLSCREEN_RENDERER_PLAYBIN_RENDERER_H__ */
