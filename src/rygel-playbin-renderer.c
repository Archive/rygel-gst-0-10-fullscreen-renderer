/*
 * Copyright (C) 2012 Openismus GmbH.
 * Copyright (C) 2012 Intel Corporation.
 *
 * Author: Jens Georg <jensg@openismus.com>
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-playbin-renderer.h"
#include "rygel-playbin-player.h"
#include <stdlib.h>
#include <string.h>
#include <glib/gi18n.h>


/**
 * A UPnP renderer that uses a GStreamer Playbin2 element.
 *
 * Using GstPlayBin2 as a model, it reflects any changes done externally, such as
 * changing the currently played URI, volume, pause/play etc., to UPnP.
 *
 * Likewise, the playbin can be modified externally using UPnP.
 *
 * You can retrieve the GstPlayBin2 by calling rygel_playbin_renderer_get_playbin().
 * You should then set the "video-sink" and "audio-sink" properties of the
 * playbin.
 *
 * Call rygel_media_device_add_interface() on the Renderer to allow it
 * to be controlled by a control point and to retrieve data streams via that
 * network interface.
 *
 * See the <link linkend="implementing-renderers-gst">Implementing GStreamer-based Renderers</link> section.
 */

G_DEFINE_TYPE (RygelPlaybinRenderer, rygel_playbin_renderer, RYGEL_TYPE_MEDIA_RENDERER)

/**
 * Create a new instance of Renderer.
 *
 * Renderer will instantiate its own instance of GstPlayBin2.
 * The GstPlayBin2 can be accessed by using rygel_playbin_player_get_playbin().
 *
 * @param title Friendly name of the new UPnP renderer on the network.
 */
RygelPlaybinRenderer*
rygel_playbin_renderer_new (const gchar *title) {
  RygelPlaybinPlayer *player = rygel_playbin_player_get_default ();
  return RYGEL_PLAYBIN_RENDERER (g_object_new (RYGEL_TYPE_PLAYBIN_RENDERER,
    "title", title,
    "player", player,
    NULL));
}

/**
 * Get the GstPlaybin2 used by this Renderer.
 */
GstElement*
rygel_playbin_renderer_get_playbin (RygelPlaybinRenderer *self) {
  RygelPlaybinPlayer *player;
  GstElement *result;

  g_return_val_if_fail (self != NULL, NULL);

  player = rygel_playbin_player_get_default ();  /* TODO: Do not return a ref from this? */
  g_return_val_if_fail (player != NULL, NULL);

  result = rygel_playbin_player_get_playbin (player);
  g_return_val_if_fail (result != NULL, NULL);

  g_object_unref (player);
  gst_object_ref (result);
  return result;
}


static void
rygel_playbin_renderer_class_init (RygelPlaybinRendererClass *klass) {
  rygel_playbin_renderer_parent_class = g_type_class_peek_parent (klass);
}

static void
rygel_playbin_renderer_init (RygelPlaybinRenderer *self G_GNUC_UNUSED) {
}
