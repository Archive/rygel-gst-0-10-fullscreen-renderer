AC_PREREQ([2.64])
AC_INIT([rygel-gst-0-10-fullscreen-renderer],
	[0.24.0],
	[http://bugzilla.gnome.org/enter_bug.cgi?product=Rygel],
	[rygel-gst-0-10-fullscreen-renderer],
	[http://live.gnome.org/Rygel])
AC_CONFIG_AUX_DIR([build-aux])

AC_CONFIG_SRCDIR([src/fullscreen-renderer.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])

AC_SEARCH_LIBS([strerror],[cposix])
AC_PROG_CC
AC_HEADER_STDC
AC_FUNC_MMAP

AC_SUBST(VERSION)

dnl Initialize automake
AM_INIT_AUTOMAKE([1.11.1 tar-ustar no-dist-gzip dist-xz -Wno-portability])
AM_MAINTAINER_MODE([enable])

dnl Initialize libtool
dnl Disable generation of static libraries
LT_PREREQ([2.2.6])
LT_INIT([dlopen disable-static])

dnl Required versions of library packages
LIBRYGEL_RENDERER_GST_REQUIRED=0.17.4
GSTREAMER_REQUIRED=0.10.36
GTK_REQUIRED=3.0.0
REQUIRED_MODULES='gio-2.0 rygel-renderer-2.4 >= $LIBRYGEL_RENDERER_GST_REQUIRED
                  gstreamer-0.10 >= $GSTREAMER_REQUIRED
                  gstreamer-plugins-base-0.10 >= $GSTREAMER_REQUIRED
                  gstreamer-video-0.10 >= $GSTREAMER_REQUIRED
                  gstreamer-interfaces-0.10 >= $GSTREAMER_REQUIRED'
PKG_CHECK_MODULES([DEPS], [$REQUIRED_MODULES])
PKG_CHECK_MODULES([GTK], [gtk+-3.0 >= $GTK_REQUIRED], [have_gtk=yes],
                  [have_gtk=no])

AM_CONDITIONAL([BUILD_UI], [test "x$[]have_gtk" = "xyes"])

dnl Debugging
AC_ARG_ENABLE(debug,
	AS_HELP_STRING([--enable-debug],[enable debugging]),,
        enable_debug=no)
if test "x$enable_debug" = "xyes"; then
        CFLAGS="$CFLAGS -O0 -g"
fi

dnl Strict compiler
AC_ARG_ENABLE(strict-cc,
	AS_HELP_STRING([--enable-strict-cc],[enable strict C compiler]),,
        enable_strict_cc=no)
if test "x$enable_strict_cc" = "xyes"; then
        CFLAGS="$CFLAGS -Wall -Werror"
fi

dnl Compile Rygel plug-in
AC_ARG_ENABLE([plugin],
    AS_HELP_STRING([--enable-plugin],[Build a Rygel plugin]),,
    [enable_plugin=no])
AM_CONDITIONAL([BUILD_PLUGIN], [test "x$[]enable_plugin" = "xyes"])
AS_IF([test "x$[]enable_plugin" = "xyes"],
      [
        RYGEL_PLUGIN_LINKER_FLAGS="`$PKG_CONFIG --variable=plugin_ldflags rygel-core-2.4`"
        AC_SUBST(RYGEL_PLUGIN_LINKER_FLAGS)

        RYGEL_PLUGIN_DIR="`$PKG_CONFIG --variable=plugindir rygel-core-2.4`"
        AC_SUBST(RYGEL_PLUGIN_DIR)

        RYGEL_VERSION="`$PKG_CONFIG --modversion rygel-core-2.4`"
        AC_SUBST(RYGEL_VERSION)
      ])

dnl Gettext
GETTEXT_PACKAGE=rygel-gst-0-10-fullscreen-renderer
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, ["$GETTEXT_PACKAGE"],
  [Define the gettext package to be used])

AM_GLIB_GNU_GETTEXT
IT_PROG_INTLTOOL([0.40.0])

dnl Shave the output
AM_SILENT_RULES([yes])

AC_CONFIG_FILES([
Makefile
src/Makefile
src/legacy-playbin.plugin
po/Makefile.in
])
AC_OUTPUT

echo "

                    ${PACKAGE} ${VERSION}
                    ============


        Prefix:                 ${prefix}
        Source code location:   ${srcdir}
        Compiler:               ${CC}
        CFLAGS:                 ${CFLAGS}
        Fullscreen renderer:    ${have_gtk}
        Plugin:                 ${enable_plugin}
"
